#!/usr/bin/python3
#
# Copyright (c) Tacsipacsi, 2023
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
r"""
Collect file names and captions of pictures of the day and store them
on a tabular data page. This is a Commons-specific script, so it
ignores the wiki settings in user-config.py.

Parameter:

-month      The month to process, in YYYY-MM format. Defaults to the
            current month.
"""

import datetime
import calendar
import json
import re

import pywikibot
from pywikibot.page import Page
from pywikibot.site import APISite

caption_lang_re = re.compile(r'Potd/\d{4}-\d{2}-\d{2} \((.+)\)')

def expand_template(site: APISite, template: str) -> str:
    expanded = site.expand_text('{{%s}}' % template).strip()
    # Silly JsonConfig doesn’t allow newlines and tabs... Hopefully we won’t break wikitext syntax.
    return expanded.replace('\n', ' ').replace('\t', ' ')

def handle_day(date: str, site: APISite, pages: set[str]) -> list:
    filename = expand_template(site, 'Potd/' + date)
    captionpages = (page for page in pages if page.startswith('Potd/%s (' % date))
    captions = {caption_lang_re.match(p).group(1): expand_template(site, p) for p in captionpages}
    return [date, filename, captions]

def main(*args: str) -> None:
    day = datetime.date.today()

    for arg in pywikibot.handle_args(args):
        if arg.startswith('-month:'):
            day = datetime.date.fromisoformat('%s-01' % arg[len('-month:'):])


    isomonth = day.strftime('%Y-%m')
    days_in_month = calendar.monthrange(day.year, day.month)[1]

    site: APISite = pywikibot.Site('commons', 'commons')
    prefix = 'Potd/%s-' % isomonth
    pages = set(p.title(with_ns=False) for p in site.allpages(prefix=prefix, namespace=10))

    pywikibot.info('Collecting data for %s...' % isomonth)
    data = list(handle_day('%s-%02d' % (isomonth, day), site, pages) for day in range(1, days_in_month + 1))

    content = {
        'license': 'CC-BY-SA-4.0',
        'description': {
            'en': 'Picture of the day data for {:%B %Y}'.format(day)
        },
        'sources': '[[Template:Potd/%s]]' % isomonth,
        'schema': {
            'fields': [
                { 'name': 'date', 'type': 'string', 'title': { 'en': 'Date' }},
                { 'name': 'filename', 'type': 'string', 'title': { 'en': 'File name' }},
                { 'name': 'caption', 'type': 'localized', 'title': { 'en': 'Caption' }}
            ]
        },
        'data': data
    }
    page = Page(site, 'Data:Potd/%s.tab' % isomonth)
    page.text = json.dumps(content)
    site.editpage(page, summary='Updating POTD data from templates', minor=False, bot=False)

if __name__ == '__main__':
    main()
