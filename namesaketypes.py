#!/usr/bin/python3
"""List classes (P31) of namesakes (P138) of items connected to some pages.

Provide a standard generator, and it will use the Wikidata items connected
to the pages yielded by the generator.

&params;
"""
#
# (C) Tacsipacsi, 2023
# Distributed under the terms of MIT License.
#

from typing import Iterator, List
from collections import Counter
import pywikibot
from pywikibot.bot import SingleSiteBot
from pywikibot.exceptions import NoPageError
from pywikibot.pagegenerators import GeneratorFactory, parameterHelp
from pywikibot.page import Page

docuReplacements = {'&params;': parameterHelp}

class NamesakeTypesBot(SingleSiteBot):
    def __init__(self, generator: Iterator[Page]) -> None:
        super().__init__(site=None, generator=generator)
        self.stats = Counter()

    def treat(self, page: Page):
        try:
            item = pywikibot.ItemPage.fromPage(page)
        except NoPageError:
            pywikibot.warning('%s has no Wikidata item' % page)
        else:
            for namesake in self.getStatementTargets(item, 'P138'):
                self.stats.update(self.getStatementTargets(namesake, 'P31'))

    def teardown(self) -> None:
        print(self.stats)

    def getStatementTargets(self, page: pywikibot.ItemPage, prop: str) -> List[pywikibot.ItemPage]:
        claims = page.claims[prop] if prop in page.claims else []
        return [claim.getTarget() for claim in claims]

def main(*args: str) -> None:
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    :param args: command line arguments
    """

    # Process global args and generator args
    gen_factory = GeneratorFactory()
    local_args = pywikibot.handle_args(args)
    gen_factory.handle_args(local_args)
    NamesakeTypesBot(gen_factory.getCombinedGenerator()).run()

if __name__ == '__main__':
    main()
